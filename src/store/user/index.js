const state = () => ({
    token: localStorage.getItem('chia-client-token') || '',
    pathStatus: localStorage.getItem('chia-client-path') || false,
})
const getters = {
    isAuth: state => !!state.token,
    isPathStatus: state => {
        return state.pathStatus === 'true' || state.pathStatus === true;
    },
}
const mutations = {
    setToken: (state, token) => {
        state.token = token;
        localStorage.setItem('chia-client-token', token);
    },
    setPathStatus: (state, status) => {
        state.pathStatus = status;
        localStorage.setItem('chia-client-path', status);
    },
    logout: (state) => {
        state.token = '';
        state.pathStatus = false;
        localStorage.removeItem('chia-client-path');
        localStorage.removeItem('chia-client-token');
    }
}
const actions = {}
export default {
    namespaced: true,
    state,
    actions,
    getters,
    mutations,
}
