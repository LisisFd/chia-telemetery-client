import { createApp } from 'vue';
import { store } from './store';
import App from './App.vue';
import router from './router';

import {
    ElInput,
    ElButton,
    ElTable,
    ElTableColumn,
    ElSelect,
    ElOption,
    ElRadio,
    ElCheckbox,
} from 'element-plus';
import 'element-plus/lib/theme-chalk/index.css';
import './assets/scss/index.scss';

const app = createApp(App);

app.component('el-input', ElInput);
app.component('el-radio', ElRadio);
app.component('el-checkbox', ElCheckbox);
app.component('el-button', ElButton);
app.component('el-table', ElTable);
app.component('el-table-column', ElTableColumn);
app.component('el-select', ElSelect);
app.component('el-option', ElOption);

app.use(store);
app.use(router);

app.mount('#app');
