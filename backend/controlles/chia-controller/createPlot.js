const PLOTS_SIZES = require('../../constants/PlotsSizes');
const DEFAULT_PLOTS_RAM_SIZES = require('../../constants/DefaultPlotsRamSizes');
const POOL_KEYS = require('../../constants/PoolKeys');
const callCLI = require('./callCLI');

module.exports = (payload) => {
    const {
        size = 32,
        ram = DEFAULT_PLOTS_RAM_SIZES[32],
        flows = 2,
        count = 1,
        buckets = 128,
        bitField = false,
        tempFolder,
        tempFolder2,
        persistentFolder
    } = payload;

    if (
        !PLOTS_SIZES.includes(size) ||
        !ram || !flows || !count || !buckets ||
        !tempFolder || !persistentFolder
    ) {
        return false;
    }

    const plotParams = {
        'plots': null,
        'create': null,
        '-k': size,
        '-b': ram,
        '-r': flows,
        '-u': buckets,
        '-n': count,
        '-t': tempFolder,
        '-d': persistentFolder,
        '-f': POOL_KEYS.farmer,
        '-p': POOL_KEYS.pool
    };

    if (bitField) {
        plotParams['-e'] = null;
    }

    if (tempFolder2) {
        plotParams['-2'] = tempFolder2;
    }

    return callCLI(plotParams);
};
