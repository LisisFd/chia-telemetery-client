const path = require('path');
const fs = require('fs');
const config = require('config');

module.exports = (newPath) => {
    const pathToLocalConfig = path.join(process.cwd(), './config/local.json');

    try {
        const data = fs.readFileSync(pathToLocalConfig);
        const parsedConfig = JSON.parse(data);

        parsedConfig.chiaCliPath = newPath;
        fs.writeFileSync(pathToLocalConfig, JSON.stringify(parsedConfig));
    } catch (error) {
        fs.writeFileSync(pathToLocalConfig, JSON.stringify({
            'chiaCliPath': newPath
        }));
    }

    config.chiaCliPath = newPath;

    config.util.loadFileConfigs();

    console.log(config);
};
