const { Harvester } = require('chia-client');
const HarvesterInstance = new Harvester();

module.exports = async () => {
    await HarvesterInstance.refreshPlots();
};
