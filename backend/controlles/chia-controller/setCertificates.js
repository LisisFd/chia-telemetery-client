const path = require('path');
const callCLIPromise = require('./callCLIPromise');

module.exports = async () => {
    const certificatesPath = path.join(process.cwd(), '/pool-certificates');
    const checkCommand = await callCLIPromise({
        'init': null,
        '-c': null,
        [certificatesPath]: null
    });

    return checkCommand.status;
};
