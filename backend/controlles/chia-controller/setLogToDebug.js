const callCLIPromise = require('./callCLIPromise');

module.exports = async () => {
    const checkCommand = await callCLIPromise({
        'configure': null,
        '--set-log-level': null,
        'DEBUG': null
    });

    return checkCommand.status;
};
