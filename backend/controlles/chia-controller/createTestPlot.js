const POOL_KEYS = require('../../constants/PoolKeys');
const callCLI = require('./callCLI');

module.exports = (tempFolder, persistentFolder) => {
    if (!tempFolder || !persistentFolder) {
        return false;
    }

    return callCLI({
        'plots': null,
        'create': null,
        '-k': 25,
        '--override-k': null,
        '-r': 2, // количество потоков
        '-b': 512, // RAM
        '-n': 1, // количество участков подряд
        '-u': 128, // количество корзин
        '-t': tempFolder, // временная директория
        '-d': persistentFolder, // финальная директория
        '-f': POOL_KEYS.farmer,
        '-p': POOL_KEYS.pool
    });
};
