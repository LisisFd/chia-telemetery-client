const { Harvester } = require('chia-client');
const getPlotMemo = require('./getPlotMemo');
const getPreparedPlotDataForServer = require('../../utils/getPreparedPlotDataForServer');
const HarvesterInstance = new Harvester();

module.exports = async () => {
    await HarvesterInstance.refreshPlots();
    const plots = await HarvesterInstance.getPlots();

    if (!plots.success) {
        return [];
    }

    const plotsData = plots.plots;

    if (!plotsData.length) {
        return [];
    }

    const preparedPlotsData = [];

    for (const plotData of plotsData) {
        preparedPlotsData.push({
            ...getPreparedPlotDataForServer(plotData),
            memo: await getPlotMemo(plotData.filename)
        });
    }

    return preparedPlotsData;
};
