const ECO_POOL_API = require('../../constants/EcoPoolApi');
const apiRequest = require('./apiRequest');
const setCookie = require('set-cookie-parser');
const state = require('../../state');

module.exports = async () => {
    let requestData;

    try {
        requestData = await apiRequest({
            method: 'get',
            url: ECO_POOL_API.pool.volume,
            token: state.token
        });
    } catch (error) {
        return { success: false };
    }

    if (requestData instanceof Error) {
        return { success: false };
    }

    const {
        status,
        data: { success }
    } = requestData;

    if (status !== 200 || !success) {
        return { success: false };
    }

    const { answer } = requestData.data;

    return {
        success: true,
        answer
    };
};
