// const axios = require('axios');
// const config = require('config');
// const debug = require('debug');
// const debugLog = debug(config.debugName + ':ecopool-controller:log');
// const debugError = debug(config.debugName + ':ecopool-controller:error');
// debugError.log = debugLog.log = console.log
//
// const ECOPOOL_API_HOST = config.main.ecopool_api_host;

const apiRequest = require('./apiRequest');
const userRegistration = require('./registration');
const userLogin = require('./login');
const userLogout = require('./logout');
const addPlots = require('./addPlots');
const plotsUploader = require('./plotsUploader');
const poolVolume = require('./poolVolume');
const userVolume = require('./userVolume');
const checkAuth = require('./checkAuth');

module.exports = {
    apiRequest,
    userRegistration,
    userLogin,
    userLogout,
    addPlots,
    plotsUploader,
    poolVolume,
    userVolume,
    checkAuth
    // async login(login, password) {
    //     const config = {
    //         method: 'post',
    //         url: `${ECOPOOL_API_HOST}/login`,
    //
    //         data:{
    //             login,
    //             password
    //         }
    //     }
    //     const authData = await axios(config).catch(e => e);
    //     if (authData instanceof Error) {
    //         debugError(authData.response.data);
    //         return {success: false}
    //     }
    //     const {status, data} = authData;
    //     if (status !== 200) {
    //         debugError(data);
    //         return {
    //             success: false,
    //         }
    //     }
    //     return {success: true, answer:{key: data.key}}
    // },
    // async logout(key){
    //
    // },
    // checkAuthKey(key){
    //
    // }
};
