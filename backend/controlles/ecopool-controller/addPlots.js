const ECO_POOL_API = require('../../constants/EcoPoolApi');
const apiRequest = require('./apiRequest');
const state = require('../../state');

module.exports = async (plots) => {
    let requestData;

    try {
        requestData = await apiRequest({
            url: ECO_POOL_API.pool.addPlots,
            data: { plots },
            token: state.token
        });
    } catch (error) {
        throw { success: false };
    }

    if (requestData instanceof Error) {
        throw { success: false };
    }

    const {
        status,
        data: { success }
    } = requestData;

    if (status !== 200) {
        throw { success: false };
    }

    if (!success) {
        throw requestData;
    }

    return { success: true };
};
