const ECO_POOL_API = require('../../constants/EcoPoolApi');
const apiRequest = require('./apiRequest');
const setCookie = require('set-cookie-parser');

module.exports = async (payload) => {
    const {
        login,
        password,
        token
    } = payload;
    let requestData;

    try {
        requestData = await apiRequest({
            url: ECO_POOL_API.login,
            data: {
                login,
                password
            },
            token
        });
    } catch (error) {
        return { success: false };
    }

    if (requestData instanceof Error) {
        return { success: false };
    }

    const {
        status,
        data: { success }
    } = requestData;
    const parseCookies = setCookie.parse(requestData.headers['set-cookie']);
    const cookieSid = parseCookies.find(cookie => {
        return cookie.name === 'connect.sid';
    });

    if (status !== 200) {
        return { success: false };
    }

    if (!success) {
        return {
            success: false,
            answer: requestData.data.answer
        };
    }

    return {
        success: true,
        answer: {
            token: cookieSid.value
        }
    };
};
