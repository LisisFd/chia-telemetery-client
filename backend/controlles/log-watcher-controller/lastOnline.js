const path = require('path');
const moment = require('moment');
const Tail = require('tail').Tail;
const pathToLogFile = path.join(process.env.HOME,'.chia', 'mainnet', 'log', 'debug.log');
const TailInstance = new Tail(pathToLogFile);
const regexToMatch = /^(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3})/;

let lastServerData = null;

TailInstance.on('line', (data) => {
    if (
        data.indexOf('farming_info') !== -1 ||
        data.indexOf('new_signage_point_harvester') !== -1
    ) {
        const dateOnline = regexToMatch.exec(data);
        if (dateOnline.length) {
            lastServerData = moment(dateOnline[0], 'YYYY-MM-DDTHH:mm:ss.sss').toDate().valueOf();
        }
    }
});

module.exports = () => {
    return lastServerData;
};
