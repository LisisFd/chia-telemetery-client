module.exports = (path) => {
    const pathPaths = path.split('/');
    const pathPathsLength = pathPaths.length;

    return pathPaths[pathPathsLength - 1];
};
