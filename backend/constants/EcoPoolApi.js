module.exports = {
    registration: '/registration',
    login: '/login',
    logout: '/logout',
    user: {
        volume: '/user/my-volume'
    },
    pool: {
        addPlots: '/add-user-plots',
        volume: '/pool/get-pool-volume'
    }
};
