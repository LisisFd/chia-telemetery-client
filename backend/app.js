const express = require('express');
const cookieParser = require('cookie-parser');
const routes = require('./routes');
const app = express();
// const chiaController = require('./controlles/chia-controller');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

routes(app);
// chiaController.init();

module.exports = app;
