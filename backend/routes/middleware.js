const createError = require('http-errors');
const config = require('config');
const UN_AUTHORIZED_ROUTES = require('../constants/UnAuthorizedRoutes');
const ROUTES_ERROR_ANSWERS = require('../constants/RoutesErrorAnswers');
const cookieExtractor = require('../utils/cookieExtractor');

module.exports = (req, res, next) => {
    if (UN_AUTHORIZED_ROUTES.includes(req.url)) {
        return next();
    }

    const ecoPoolKey = cookieExtractor(req, config.main.cookiesName);

    if (!ecoPoolKey) {
        return next(createError(403, ROUTES_ERROR_ANSWERS[403]));
    }

    return next();
}
