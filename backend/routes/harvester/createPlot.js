const { queues } = require('../../state');
const ChiaController = require('../../controlles/chia-controller');
const GuiController = require('../../controlles/gui-controller');

module.exports = async (req, res, next) => {
    const {
        body: { plotParams }
    } = req;

    GuiController.addPlotsQueue(plotParams);

    return res.send('1234');
    // const child = await ChiaController.createTestPlot('/Users/anton/Desktop/chia_temp/', '/Users/anton/Desktop/chia_temp/');

    // console.log(child);

    // new Promise((resolve, reject) => {
    //     let dataSent = false;
    //
    //     child.stderr.on('data', function (data) {
    //         if (!dataSent) {
    //             dataSent = true;
    //             reject(data.toString().trim());
    //         }
    //     });
    //     child.stdout.on('data', function (data) {
    //         if (!dataSent) {
    //             dataSent = true;
    //             resolve(true);
    //         }
    //     });
    // })
    //     .then(() => {
    //         res.json({
    //             success: true,
    //             answer: true
    //         })
    //     })
    //     .catch((err) => {
    //         res.json({
    //             success: false,
    //             err
    //         });
    //     });
};
