const express = require('express');
const router = express.Router();

const { Harvester } = require('chia-client');

const harvesterInstance = new Harvester();

const config = require('config');
const debug = require('debug');
const debugLog = debug(`${config.debugName}:routes:farms:log`);
const debugError = debug(`${config.debugName}:routes:farms:error`);
debugLog.log = debugError.log = console.log;

/* GET home page. */
router.get('/', async (req, res, next) => {
    const plots = await harvesterInstance.getPlots();

    if (!plots.success){
        debugError(plots);
    }
    res.render('farm', {plots: plots.plots});
});

module.exports = router;
