const express = require('express');
const router = express.Router();
const userSetChia = require('./setChia');
const userVolume = require('./volume');

router.post('/set-chia', userSetChia);
router.get('/volume', userVolume);

module.exports = router;
