const ChiaController = require('../../controlles/chia-controller');

module.exports = async (req, res, next) => {
    const {
        body: { path }
    } = req;

    if (!path) {
        return { success: false };
    }

    const checkCLI = await ChiaController.checkCLI(path);

    if (checkCLI) {

        ChiaController.setCLIPath(path);
        await ChiaController.init();

        return res.send({
            success: true
        });
    } else {
        return res.send({ success: false });
    }
};
