const express = require('express');
const router = express.Router();
const EcoPoolController = require('../../controlles/ecopool-controller');
const ChiaController = require('../../controlles/chia-controller');
const state = require('../../state');

router.post('/', async (req, res, next) => {
    const {
        body: {
            login,
            password,
            token
        }
    } = req;
    const serverToken = req.cookies.token;

    if (token && serverToken !== token) {
        return res.send({ success: false });
    }

    let requestData;

    try {
        requestData = await EcoPoolController.userLogin({
            login,
            password,
            token: serverToken
        });
    } catch (error) {
        return res.send(error);
    }

    const { success } = requestData;

    if (!success) {
        return res.send(requestData);
    }

    const { answer } = requestData;
    const checkChia = await ChiaController.checkCLI();

    state.token = answer.token;
    requestData.answer.checkChia = checkChia;

    if (checkChia) {
        await ChiaController.init();
        return res.send(requestData);
    } else {
        return res.send(requestData);
    }
});

module.exports = router;
