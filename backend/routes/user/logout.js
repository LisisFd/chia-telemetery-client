const express = require('express');
const router = express.Router();
const EcoPoolController = require('../../controlles/ecopool-controller');

router.get('/', async (req, res, next) => {
    let requestData;

    try {
        requestData = await EcoPoolController.userLogout();
    } catch (error) {
        return res.send({ success: false });
    }

    return res.send(requestData);
});

module.exports = router;
